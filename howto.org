# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: "config.org"
#+REVEAL_PREAMBLE: <div class="legalese"><p><a href="/imprint.html">Imprint</a> | <a href="/privacy.html">Privacy Policy</a></p></div>

#+TITLE: How to create presentations with emacs-reveal
#+AUTHOR: Jens Lechtenbörger Test
#+DATE: April 2019 (emacs-reveal 3.0.0 and later)

* Presentation Hints
** General
   - This is a [[https://revealjs.com][reveal.js]] presentation
     - See [[https://oer.gitlab.io/hints.html][usage hints for emacs-reveal presentations]]
   - Key bindings and navigation
     - Press “?” to see key bindings of reveal.js
       - In general, “n” and “p” move to next and previous slide; mouse
	 wheel works as well
       - Search with Ctrl-Shift-F
     - Up/down (swiping, arrows) move within sections,
       left/right jump between sections (type “o” to see what is where)
     - Type slide’s number followed by Enter to jump to that slide
     - Browser history (buttons, Alt-CursorLeft, Alt-CursorRight)
     - Zoom with Ctrl-Mouse or Alt-Mouse

** Offline work
   - Students often ask for download-able presentations
   - Alternatives
     1. Clone repository, build presentations locally (see [[#usage][Usage]])
     2. Download build artifacts from
        [[https://gitlab.com/oer/emacs-reveal-howto/pipelines][recent pipeline]]
        (if not expired)
     3. Generate PDF
        - Why, really?
	  - Why not download source files instead?
	  - [[https://orgmode.org/][Org mode]], which is plain text
	- Change the URL by adding “?print-pdf” after “.html”,
	  then print to PDF file (usually, Ctrl-p)
	- Alternatively, generate PDF via LaTeX from Org
	  source file
          - Replace ~.html~ (and whatever follows) in address bar of
            browser with ~.pdf~

** Audio
   :PROPERTIES:
   :reveal_extra_attr: data-audio-src="./audio/Tours_-_01_-_Enthusiast.ogg"
   :END:
   - Audio playback should start automatically
     - Currently, you should be hearing Enthusiast by
       [[https://freemusicarchive.org/music/Tours/][Tours]]
       - Licensed under
  	 [[https://creativecommons.org/licenses/by/3.0/][Creative Commons Attribution 3.0 Unported (CC BY 3.0)]]
       - Converted to [[https://en.wikipedia.org/wiki/Ogg][free Ogg format]] with [[https://www.audacityteam.org/][Audacity]]
     - See [[https://github.com/rajgoel/reveal.js-plugins/tree/master/audio-slideshow#user-content-compatibility-and-known-issues][compatibility and known issues of the underlying audio plugin]]
       - [[https://www.mozilla.org/en-US/firefox/][Firefox]],
         which I recommend as browser in general
         ([[https://blogs.fsfe.org/jens.lechtenboerger/2015/06/09/three-steps-towards-more-privacy-on-the-net/][here in English]]
         and [[https://www.informationelle-selbstbestimmung-im-internet.de/][here in German]]),
         seems to work everywhere
   - Audio controls are shown at bottom left

** (Speaker) Notes
   # The simplest form of macro reveallicense() occurs on
   # [[#what][a slide below]].  On this slide and below, it shows an
   # inline image with vertically aligned, rotated license statement.
   # Here, the fourth argument is t, which indicates that shortened
   # license information should be displayed.  This is particularly useful
   # for CC0 (Public Domain) work, which does not require attribution
   # but where others might still want to know that it can be reused
   # under less restrictive conditions than the overall work.
   - Slides contain additional notes as plain text if you see the
     folder icon at the top right (as on this slide)
     {{{reveallicense("./figures/icons/folder_inbox.meta","10vh",nil,t)}}}
     - Either press “v” to see the “courseware view”
       or click on that icon or press “s” to see the “speaker notes view”
     - You need to allow pop-ups
       - If the pop-up window does not work, you may need to press “s”
         twice or close the pop-up window once
#+BEGIN_NOTES
These are sample notes
- Lists can be used here
- You can time your presentation
  - Maybe look at [[https://gitlab.com/lechten/talks-2018/blob/master/2018-04-24-Blockchain.org][one]] of my talks to see how to define timing
#+END_NOTES

* Introduction

** What’s This?
   :PROPERTIES:
   :CUSTOM_ID: what
   :END:
   # Note the use of macro reveallicense() below to show an inline image
   # with vertically aligned, rotated license statement.
   # Alternatively, one could also use standard Org mode features and
   # replace the macro with a #+CAPTION, which would be displayed
   # underneath the image.
   # Again alternatively, later as part of slide figure-with-meta-data,
   # you can see the use of macro revealimg(), to display a
   # horizontally centered image with caption and license statement.
   # The immediately following slide describes the required format of
   # meta-data.
   # Both macros are defined in config.org of oer-reveal.
   - ~Emacs-reveal~ is [[https://fsfe.org/about/basics/freesoftware.en.html][free software]]
     to generate [[https://revealjs.com/][reveal.js]] presentations (slides with audio) from simple text
     files in [[https://orgmode.org/][Org mode]]
     {{{reveallicense("./figures/3d-man/board.meta","30vh")}}}
     - Benefits
       - For your audience
         - Self-contained presentations embedding audio
         - Usable on lots of (including mobile and offline) devices with
           just a browser
       - For you as producer
         - Separation of layout and contents
	   (similarly to, e.g., LaTeX)
         - Simple text format allows diff and merge for ease of collaboration

** Prerequisites
  - I suppose (and strongly recommend) that you use GNU/Linux
    ([[https://getgnulinux.org/switch_to_linux/try_or_install/][help on getting started]])
    - Actually, not much here is operating system specific
  - ~Emacs-reveal~ should really be used with the text editor [[https://www.gnu.org/software/emacs/][GNU Emacs]]
    - (You could try other editors and build presentations within
      GitLab, thanks to GitLab’s infrastructure)
      - (In fact, you do not need an editor at all but could edit
        presentations using a Web browser on ~GitLab.com~, e.g., with the
        [[https://gitlab.com/-/ide/project/oer/emacs-reveal-howto/edit/master/][Web IDE]]
        (requires login))

** Installation and Quickstart
   - ~Emacs-reveal~ builds upon Gnu Emacs with
     [[https://orgmode.org][Org mode]]
     - [[https://gitlab.com/oer/emacs-reveal][~Emacs-reveal~ is
       available as free software on GitLab]]
   - You also need Git
     - [[https://git-scm.com/book/en/v2/Getting-Started-Installing-Git][Getting started]]
       - The [[https://git-scm.com/book/en/v2][Pro Git book]] is a great source in general
     - [[https://oer.gitlab.io/oer-courses/cacs/Git-Introduction.html][Git introduction as OER]]
       (created with ~emacs-reveal~)

*** Installation of ~emacs-reveal~
    :PROPERTIES:
    :CUSTOM_ID: git-install
    :END:
    - Starting with version 1.0.0, you can install ~emacs-reveal~
      in a directory of your choice
      1. Choose a directory, e.g., =~/.emacs.d/elpa=, and clone software
         - =cd ~/.emacs.d/elpa=
         - ~git clone https://gitlab.com/oer/emacs-reveal.git~
      2. Install required Emacs packages from [[https://melpa.org/][MELPA]]
         - ~emacs --batch --load emacs-reveal/install.el --funcall install~
      3. Maybe add lines like this to =~/.emacs=
         - =(add-to-list 'load-path "~/.emacs.d/elpa/emacs-reveal")=
         - =(require 'emacs-reveal)=
           - Beware! This changes values of other packages without warning.
           - You may skip step (3) and restrict those changes to export runs.

*** Quickstart with ~emacs-reveal~
    - E.g., generate this howto
      1. Choose a directory, clone howto
         - ~git clone https://gitlab.com/oer/emacs-reveal-howto.git~
         - ~cd emacs-reveal-howto/~
      2. Install submodules (the howto embeds a repository for figures)
         - ~git submodule sync --recursive~
         - ~git submodule update --init --recursive~
      3. Generate the HTML presentation from Org source ~howto.org~
         - ~emacs --batch --load elisp/publish.el~
         - Publication code needs to be able to locate ~emacs-reveal.el~
           - Code in ~elisp/publish.el~ tries (a) load path after adding
             =~/.emacs.d/elpa/emacs-reveal= (suggested as directory
             for ~emacs-reveal~ on previous slide) and
             (b) subdirectory ~emacs-reveal~

* Usage
  :PROPERTIES:
  :CUSTOM_ID: usage
  :END:

** Alternatives
   1. Create presentations locally on Command Line
   2. Create presentations in GNU Emacs
   3. Create presentations with [[https://www.docker.com/][Docker]]
      {{{reveallicense("./figures/logos/docker-horizontal.meta","10vh")}}}
      - [[https://gitlab.com/oer/docker][Docker image with emacs-reveal]]
        - Similarly to previous alternative; necessary software bundled
        - See [[https://gitlab.com/oer/emacs-reveal/blob/master/README.md][README of emacs-reveal]]
        - [[https://oer.gitlab.io/oer-courses/vm-neuland/04-Docker.html][Introduction to Docker]],
          built with ~emacs-reveal~
   4. Create and publish presentations on [[https://about.gitlab.com/][GitLab]]
      {{{reveallicense("./figures/logos/GitLab-wm_no_bg.meta","10vh")}}}
      - Based on [[https://docs.gitlab.com/ce/ci/][GitLab Continuous Integration infrastructure]]
        and above Docker image

** Build Presentations on Command Line
   1. Create Org file in directory ~emacs-reveal-howto~
      - See contained source file for this presentation, ~howto.org~
   2. Build presentations for files ending in ~.org~
      - (Except internal ones, see function ~oer-reveal-publish-all~)
      - ~emacs --batch --load elisp/publish.el~
        - Presentations are built in subdirectory ~public/~
   3. Open presentation in [[https://www.mozilla.org/en-US/firefox/][Firefox]]
      - E.g.: ~firefox public/howto.html~
   4. Optional: Copy ~public/~ to public web server

** Building Presentations in Emacs
   1. Generate HTML presentation for visited
      ~.org~ file using Org export functionality: @@html:<br>@@
      Press ~C-c C-e v b~
      - This generates HTML file in current directory and opens it
	in default browser
      - For this to work
        1. Settings of ~emacs-reveal~ should be in effect
           (~emacs-reveal.el~ is loaded, e.g., with
           [[#git-install][step (3) above]])
        2. Necessary resources, in particular
	  ~reveal.js~, must be accessible in ~.org~ file’s
	  directory
           - I use
             ~emacs --batch --load elisp/publish.el~
             once to populate ~public/~, then create a symbolic link:

             ~ln -s public/reveal.js~
        3. For image grids, you may need:
           ~(setq oer-reveal-export-dir "./")~

** Build Presentations on GitLab
   1. Fork [[https://gitlab.com/oer/emacs-reveal-howto][emacs-reveal-howto]]
      on GitLab ([[https://docs.gitlab.com/ce/workflow/forking_workflow.html][fork documentation]])
      - ~git clone <the URL of YOUR GitLab project>~
   2. Create or update Org files in cloned directory
      - Push changes to your fork
   3. GitLab infrastructure picks up changes and publishes presentations as
      [[https://about.gitlab.com/product/pages/][GitLab Pages]]
      - Based on Continuous Integration (CI) infrastructure
        - Configured by file
          [[https://gitlab.com/oer/emacs-reveal-howto/blob/master/.gitlab-ci.yml][.gitlab-ci.yml]]
      - CI run takes some minutes
      - Go to Settings → Pages to see the Pages’ address

* Some Presentation Features
  :PROPERTIES:
  :CUSTOM_ID: features
  :END:

** Text Slide
   - A list
   - With a sub-list whose items appear
     #+ATTR_REVEAL: :frag (appear)
     - This is /emphasized/
     - This is *bold*
     - This ~looks like code~
     - This is [[color:green][green]]
     - Nothing special

** Some Fragment Styles
   #+ATTR_REVEAL: :frag (gray-out shrink grow highlight-red)
   - Forget
   - Shrink
   - Grow
   - Very important

*** Fragments with Custom Order
    #+ATTR_REVEAL: :frag (appear) :frag_idx (1 4 3 2 1)
    * I’m first.
    * Fourth.
    * Third.
    * Second.
    * I’m also first.

** Centered Text

   #+ATTR_HTML: :class org-center
   Just some horizontally centered text.  Created by assigning a class
   with ~text-align: center~.

** On Sections
   # As explained in the Org manual, link targets can have different
   # forms.  The first link below points to #features, which has been
   # defined above as CUSTOM_ID.  The second link uses the section
   # header’s text as link target.
   - This slide is part of section [[#features][Some Presentation Features]]
     - We can link to slides, e.g., [[Text Slide][an earlier slide]]
       - You can use the browser history to go back
     - Side note: Check source code to see two variants of link
       targets used on this slide
   # The following directive with “appear” lets the next thing
   # appear in its entirety; instead of each list item individually as
   # on the previous slide with “(appear)” in parentheses.
   #+ATTR_REVEAL: :frag appear
   - This slide can also be perceived as its own subsection
     - The [[#another-anchor][next slide]] is on a deeper level of nesting
   - (This list item appears simultaneously with previous bullet point)

*** Another Slide
    :PROPERTIES:
    :CUSTOM_ID: another-anchor
    :END:
    - This slide is on a deeper level of nesting
    - This level of nesting is not shown in the table of contents in
      the slide’s bottom
    - By the way, the headings in the table of contents below are
      hyperlinks
      - And your browser remembers the history, back/forward buttons
        and shortcuts should work
      - Mousewheel and swiping work

** Two Columns: Pro/Con of emacs-reveal
   #+ATTR_REVEAL: :frag appear
   #+BEGIN_leftcol
   Pro
   #+ATTR_REVEAL: :frag (appear)
   - Free/libre open source software
   - Device-independent presentations
     - Also mobile and offline
     - Generated from simple text format
       - Easy to learn
       - Collaboration with diff/merge/git
       - Separation of layout and content
   #+END_leftcol
   #+ATTR_REVEAL: :frag appear
   #+BEGIN_rightcol
   Con
   #+ATTR_REVEAL: :frag (appear)
   - No [[https://en.wikipedia.org/wiki/WYSIWYG][WYSIWYG]]
   - (Need to learn something new)
   #+END_rightcol

* Figures and Audio

** Slide with Figure and Audio
   :PROPERTIES:
   :reveal_extra_attr: data-audio-src="./audio/Tours_-_01_-_Enthusiast.ogg"
   :END:
   - This figure is part of a
     [[https://oer.gitlab.io/OS/Operating-Systems-Memory-II.html][different presentation]]
     {{{reveallicense("./figures/OS/clock-steps.meta","40vh",nil,'none)}}}
     - Notice: No license displayed for figure → License of document applies
   - The song Enthusiast by
     [[https://freemusicarchive.org/music/Tours/][Tours]]
     is licensed under
     [[https://creativecommons.org/licenses/by/3.0/][Creative Commons Attribution 3.0 Unported (CC BY 3.0)]]


** Figure with Caption and License
   :PROPERTIES:
   :CUSTOM_ID: figure-with-meta-data
   :END:
   - Display image with meta-data specified in file
     - Simplify sharing of images with source and license
   - Functionality and meta-data format are specific
     to ~emacs-reveal~
     - See next slide for sample file

   {{{revealimg("./figures/3d-man/decision.meta","To share or not to share","30vh")}}}

*** Meta-Data File for Previous Image

#+INCLUDE: "./figures/3d-man/decision.meta" src emacs-lisp

** An Image Grid: Computers

{{{revealgrid(42,"./figures/devices/computer.grid",60,4,3,"\"ga1 ga2 ga2 ga3\" \"ga1 ga4 ga5 ga6\" \"ga7 ga8 ga9 ga9\"")}}}

*** Creation of Previous Image Grid
    - Single line in source file, using macro ~revealgrid~
      #+BEGIN_SRC
{{{revealgrid(42,"./figures/devices/computer.grid",60,4,3,"\"ga1 ga2 ga2 ga3\" \"ga1 ga4 ga5 ga6\" \"ga7 ga8 ga9 ga9\"")}}}
      #+END_SRC
      - Arguments explained in
        [[https://gitlab.com/oer/oer-reveal/blob/master/org/config.org][config.org of oer-reveal]]
      - With file ~computer.grid~ as follows
        #+INCLUDE: "./figures/devices/computer.grid" src emacs-lisp

** Appearing Items with Audio
   (Audios produced with
   [[https://github.com/marytts/marytts][MaryTTS]],
   converted to Ogg format with [[https://www.audacityteam.org/][Audacity]])

   #+ATTR_REVEAL: :frag (appear) :audio (./audio/1.ogg ./audio/2.ogg ./audio/3.ogg)
   - One
   - Two
   - Three

* Misc
** Quiz Plugin
   :PROPERTIES:
   :CUSTOM_ID: quiz-support
   :END:
   - ~Emacs-reveal~ embeds this
     [[https://gitlab.com/schaepermeier/reveal.js-quiz][quiz plugin]]
     - [[https://schaepermeier.gitlab.io/reveal-quiz-demo/demo.html][Demo of plugin’s author]]
   - In presentations, quizzes support active learning
     - In particular, retrieval practice

*** Sample Quiz
#+REVEAL_HTML: <script data-quiz src="./quizzes/sample-quiz.js"></script>

#+MACRO: klipse-languages (eval (message "%s" (mapconcat #'identity org-re-reveal-klipse-languages ", ")))
** Klipse for Code Evaluation
   :PROPERTIES:
   :CUSTOM_ID: klipse-support
   :END:
   - Org-re-reveal supports [[https://github.com/viebel/klipse][Klipse]]
     - Teach programming
       - Code changes in upper part result in output changes in lower part
     - Browser-side code evaluation for various programming languages
       - See ~org-re-reveal-klipse-languages~ for supported subset
         - {{{klipse-languages}}}
       - Either variable ~org-re-reveal-klipsify-src~ needs to be
         ~non-nil~ or code block needs to have ~#+ATTR_REVEAL:
         :klipsify t~
         - (You can also specify ~:klipse-width~ and ~:klipse-height~)
       - Due to an [[https://github.com/viebel/klipse/issues/334][issue in Klipse]],
         you may need to use an older or non-minified version of Klipse
   - Code on next two slides copied from
     [[https://github.com/yjwen/org-reveal/blob/master/Readme.org][README of Org-Reveal]]

*** HTML Src Block
#+ATTR_REVEAL: :klipsify t :klipse-height 200px
#+BEGIN_SRC html
<h1 class="whatever">hello, what's your name</h1>
#+END_SRC

- Above code block activates Klipse via ~#+ATTR_REVEAL: :klipsify t~
  - Regardless of ~org-re-reveal-klipsify-src~
- Subsequent code blocks do not do so

*** Javascript Src Block
#+BEGIN_SRC js
console.log("success");
var x='string using single quote';
x
#+END_SRC

*** PHP Src Block
#+BEGIN_SRC php
$name = "Alice";
echo "Moin " . $name . "!";
#+END_SRC

*** Python Src Block
#+BEGIN_SRC python
def factorial(n):
    if n < 2:
        return 1
    else:
        return n * factorial(n - 1)

print(factorial(10))
#+END_SRC

* The End
** Further Reading
   - [[https://orgmode.org/#docs][Manuals and tutorials for Org mode]]
   - [[https://oer.gitlab.io/OS/][Presentations for a course on Operating Systems]]
     demonstrating more features of Org mode (e.g., table of contents
     as agenda, bibliography with citations, keyword index, PDF export) and
     reveal.js (e.g., notes, animated SVGs)

** Go for it!

   {{{revealimg("./figures/3d-man/steps.meta","The road ahead …")}}}

   [[https://gitlab.com/oer/]]

#+MACRO: copyrightyears 2017, 2018, 2019
#+MACRO: licensepreamble
#+INCLUDE: "~/.emacs.d/oer-reveal-org/license-template.org" :minlevel 2

# Local Variables:
# indent-tabs-mode: nil
# End:
